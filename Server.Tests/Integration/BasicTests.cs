using System;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Server;
using System.Threading.Tasks;

namespace Server.Tests.Integration
{
	public class BasicTests :
		IClassFixture<WebApplicationFactory<Server.Startup>>
	{
		private readonly WebApplicationFactory<Server.Startup> _factory;

		public BasicTests( WebApplicationFactory<Server.Startup> factory )
		{
			_factory = factory;
		}

		[Theory]
		[InlineData( "/api/jokes" )]
		public async Task Get_EndpointsReturnSuccessAndCorrectContentType( string url )
		{
			// Arrange
			var client = _factory.CreateClient();

			// Act
			var response = await client.GetAsync( url );

			// Assert
			response.EnsureSuccessStatusCode(); // Status Code 2xx
			Assert.Equal( "application/json; charset=utf-8", response.Content.Headers.ContentType.ToString() );
		}
	}
}
