﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Server.Models
{
	[Table( "Joke" )]
	public class Joke
	{
		[Key]
		[DatabaseGeneratedAttribute( DatabaseGeneratedOption.Identity )]
		public ulong Id { get; set; }

		[Required]
		public string Text { get; set; }
	}
}
