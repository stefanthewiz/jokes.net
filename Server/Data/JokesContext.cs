﻿using System;
using Microsoft.EntityFrameworkCore;
using Server.Models;

namespace Server.Data
{
	public class JokesContext : DbContext
	{
		public JokesContext( DbContextOptions<JokesContext> options ) : base( options )
		{
		}

		public DbSet<Joke> Jokes { get; set; }

	}
}
