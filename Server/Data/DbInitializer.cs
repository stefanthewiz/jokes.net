﻿using System;
using System.Linq;
using Server.Models;
using Microsoft.EntityFrameworkCore;

namespace Server.Data
{
	public class DbInitializer
	{
		public static void Initialise( JokesContext context )
		{
			// reset db
			context.Database.EnsureDeleted();
			context.Database.EnsureCreated();

			// add some jokes
			context.Jokes.Add( new Joke()
			{
				Text = "Penguins are cool"
			} );

			context.SaveChanges();
		}
	}
}
