﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Server.Data;

namespace Server
{
	public class Startup
	{
		public Startup( IConfiguration configuration )
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices( IServiceCollection services )
		{
			// db settings
			string ConnectionString = GetDBConnectionString();
			services.AddDbContext<JokesContext>( options => options.UseMySql( ConnectionString ) );

			services.AddMvc().SetCompatibilityVersion( CompatibilityVersion.Version_2_2 );
		}

		private string GetDBConnectionString()
		{
			/* You can set a DB connection string as (in order of priority):
			 * - 'MYSQLCONNSTR_DEFAULT=str' in environment variables (or other prefixes depending on
			 *   which provider you are using: SQLCONNSTR_, SQLAZURECONNSTR_, CUSTOMCONNSTR_ );
			 * - same variable as above, but set on the command line instead;
			 * - in appsettings.json as { "ConnectionStrings": { "Default": "str" } }
			 * 
			 * NB: The configuration keys are case-INsensitive
			 */
			string result = Configuration.GetConnectionString( "Default" );
			return result;
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure( IApplicationBuilder app, IHostingEnvironment env )
		{
			if( env.IsDevelopment() )
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
