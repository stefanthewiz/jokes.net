﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Data;
using Server.Models;

namespace Server.Controllers
{
	[Route( "api/[controller]" )]
	[ApiController]
	public class JokesController : ControllerBase
	{
		private readonly JokesContext _context;
		private readonly static Random _random = new Random();

		public JokesController( JokesContext context )
		{
			_context = context;
		}

		// GET api/jokes
		[HttpGet]
		public IEnumerable<Joke> Get()
		{
			var result = _context.Jokes.ToList();
			return result;
		}

		// GET api/jokes/random
		[HttpGet( "random" )]
		public IEnumerable<Joke> GetRandom()
		{
			var result = new List<Joke>();
			int jokeCount = _context.Jokes.Count();
			if( jokeCount > 0 )
			{
				int skip = ( int ) ( jokeCount * _random.NextDouble() );
				if( skip > 0 )
				{
					result = _context.Jokes.OrderBy( j => j.Id ).Skip( skip ).Take( 1 ).ToList();
				}
				else
				{
					result = _context.Jokes.OrderBy( j => j.Id ).Take( 1 ).ToList();
				}
			}
			return result;
		}

		// GET api/jokes/5
		[HttpGet( "{id}" )]
		public ActionResult<Joke> Get( ulong id )
		{
			Joke result = _context.Jokes.Find( id );

			if( result != null )
			{
				return result;
			}

			return NotFound();
		}

		// POST api/jokes
		[HttpPost]
		public void Post( [FromBody] string value )
		{
		}

		// PUT api/jokes/5
		[HttpPut( "{id}" )]
		public void Put( int id, [FromBody] string value )
		{
		}

		// DELETE api/jokes/5
		[HttpDelete( "{id}" )]
		public void Delete( int id )
		{
		}
	}
}
